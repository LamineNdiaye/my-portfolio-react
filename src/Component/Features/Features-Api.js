const data = [
  {
    id: 1,
    image: "https://img.icons8.com/ios/50/000000/application-window.png",
    title: "Web Site",
    desc: " I create responsive websites. I help companies to have visibility on the web.",
  },
  {
    id: 2,
    image: "https://img.icons8.com/ios/50/000000/adobe-illustrator--v1.png",
    title: "Graphic Design",
    desc: "I create logos, business cards, website models.",
  },
  {
    id: 3,
    image: "https://img.icons8.com/ios/50/000000/wordpress--v1.png",
    title: "Wordpress",
    desc: "I create e-commerce sites, showcase sites, e-media sites.",
  },
  {
    id: 4,
    image: "https://img.icons8.com/ios/50/000000/adobe-premiere-pro--v1.png",
    title: "Vidéo Editing",
    desc: "I do video editing and film.",
  },
  {
    id: 5,
    image: "https://img.icons8.com/ios/50/000000/polyline.png",
    title: "Personal Portfolio April",
    desc: " It uses a dictionary of over 200 Latin words, combined with a handful of model sentence.",
  
    },
  {
    id: 6,
    image: "https://img.icons8.com/material-rounded/24/000000/wifi--v1.png",
    title: "CEO Marketing",
    desc: "always free from repetition, injected humour, or non-characteristic words etc.",

  },
];
export default data;
